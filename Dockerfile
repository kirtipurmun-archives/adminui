# AdminUI Build Dockerfile

# This Dockerfile will build the create-react-app based AdminUI, baking in the
# API endpoint url and publishing a production-ready version for simple HTTP
# serving.

FROM node:latest AS build
WORKDIR /home/node/app
COPY ./ ./
RUN ["npm", "install"]
RUN ["npm", "run", "build"]

FROM nginx:latest
WORKDIR /usr/share/nginx/html
COPY --from=build /home/node/app/build ./
