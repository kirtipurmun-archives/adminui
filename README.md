# Admin UI
This is a web-based user interface for the [Municipality Project Tracker API](https://bitbucket.org/kirtipurmun/api/). It has been created using [React](https://reactjs.org/) and [Material UI](https://material-ui.com/) for the management of projects within Kirtipur Municipality.

## Introduction
This is one part of a larger system that the [Municipality Project Tracker (MPT)](https://bitbucket.org/kirtipurmun/mpt/) uses, and so it is advised that you read the Getting Started section of the MPT readme for more information. This admin interface has been designed to make the tracking of projects, activities and budget allocation easy to access, modify and report.
