import React from 'react';
import { BrowserRouter as Router,  Route } from 'react-router-dom';
import Footer from '../Layouts/Footer'
import ScrollToTop from '../Layouts/ScrollToTop'
import Projects from '../Projects'
import Project from '../Project'

const App = () => (
  <Router>
    <ScrollToTop>
      <Route exact path="/" component={Projects} />
      <Route exact path="/projects/new" component={Project} />
      <Route path="/projects/:projectName/:projectId" component={Project} />
      <Footer />
    </ScrollToTop>
  </Router>
)

export default App
