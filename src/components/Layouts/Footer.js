import React from 'react';
import { makeStyles, Typography, Container, Grid, Link } from '@material-ui/core'
import logoImage from '../../assets/images/mptlogo-dark-vector.svg'

const useStyles = makeStyles(theme => ({
    Footer: { 
      backgroundColor: theme.palette.grey[800],
      marginTop: theme.spacing(4),
      paddingTop: theme.spacing(4),
      paddingBottom: theme.spacing(4),
    },
    Titles: {
      color: theme.palette.grey[100]
    },
    logoImageHeading: {
      marginTop: 0,
      marginBottom: 0
    },
    logoImage: {
      width: '200px'
    }
  }))

export default function Footer() {
  const styles = useStyles();
  return (
    <footer className={styles.Footer}>
      <Container maxWidth="lg">
        <Grid container>
          <Grid item md={2} sm={12}>
              <h2 className={styles.logoImageHeading}><img className={styles.logoImage} src={logoImage} alt="Municipality Project Tracker logo" /></h2>
              <Typography className={styles.Titles} variant="caption" component="h3">An initiative of Kirtipur Municipality</Typography>
          </Grid>
          <Grid item md={6} sm={12}>
            <ul>
              <li><Link href="/" color="secondary" title="List all projects">Projects</Link></li>
              <li><Link href="/orgs" color="secondary" title="List all committees">Committees</Link></li>
              <li><Link href="/reports" color="secondary" title="See reports">Reports</Link></li>
            </ul>
          </Grid>
          <Grid item md={4}>
              <Typography className={styles.Titles} variant="h6" component="h3">Find out more</Typography>
              <Typography className={styles.Titles} variant="body2" component="p">This software has been developed for Kirtipur Municipality to help manage project schedules and budget allocations. It is open-source and we welcome its usage in other municipalities.</Typography>
              <Typography className={styles.Titles} variant="body2" component="p">Visit <Link href="https://bitbucket.org/account/user/kirtipurmun/projects/MPT" color="secondary" title="See this open-source project on BitBucket">Municipality Project Tracker's Project</Link> on BitBucket to see design, development and implementation details.</Typography>
              <Typography className={styles.Titles} variant="body1" component="p">&copy; Kirtipur Municipality 2019</Typography>
          </Grid>
        </Grid>
      </Container>
    </footer>
  );
}
