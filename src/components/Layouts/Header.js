import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';


export default function SearchAppBar(props) {
  return (
      <AppBar position="static">
        <Toolbar>
          {props.children || <Typography variant="h6" noWrap>
            {props.title}
          </Typography>}
        </Toolbar>
      </AppBar>
  );
}
