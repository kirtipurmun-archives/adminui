import React, { useState, useEffect } from 'react';

import MaterialTable from 'material-table';

import axios from 'axios';
import uuid from 'uuid/v4';
import slug from 'slug';

import { 
  makeStyles,
  Container,
  Grid,
  Paper,
  AppBar,
  Toolbar,
  Typography,
  Button,
  MenuItem,
  Select,
  InputLabel,
  FormControl,
  TextField,
  IconButton,
  Snackbar,
  CircularProgress
} from '@material-ui/core';


import { green } from '@material-ui/core/colors';
import { Save, Restore, ArrowBack, Close } from '@material-ui/icons';

const MPT_API_ENDPOINT = process.env.REACT_APP_MPT_API_ENDPOINT;
const NEW_PROJECT_SHELL = { name: '', status: 'pending', activities: [] };

const useStyles = makeStyles(theme => ({
  SuccessSnackBar: {
    backgroundColor: green[600],
  },
  rightIcon: {
    marginLeft: theme.spacing(1),
  },
  iconSmall: {
    fontSize: 20,
    marginRight: 4
  },
  BackButton: {
    marginRight: theme.spacing(1),
    color: 'white'
  },
  AppBarGroup: {
    display: 'flex',
    alignItems: 'center',
    maxWidth: '50%' 
  },
  Paper: {
    marginTop: theme.spacing(4),
    marginBottom: theme.spacing(4),
    padding: theme.spacing(4)
  },
  FormField: {
    display: 'flex'
  },
  AlignWithSelect: {
    width: '100%',
    position: 'relative',
    top: '8px',
  },
  FormItem: {
    width: '100%',
  },
  loadingButton: {
    position: 'relative'
  },
  LoadingIndicator: {
    color: theme.palette.primary.contrastText,
    position: 'absolute',
    left: '50%',
    top: '50%',
    marginLeft: '-12px',
    marginTop: '-12px',
  }
}));

export default function Project(props) {
  const [originalProject, setOriginalProject] = useState();
  const [project, setProject] = useState({});
  const [hasChanged, setHasChanged] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [isSaved, setIsSaved] = useState(false);

  useEffect(() => {
    async function getProject() {
      const { match : { params: { projectId }} } = props;
      const isNew = !projectId && props.match.path.split('/').pop() === 'new';     
      
      setHasChanged(false);
      setIsLoading(false);
      setIsSaved(false);
      if (isNew) {
        setProject(NEW_PROJECT_SHELL);
        return;
      }

      const projectRequest = await axios.get(`${MPT_API_ENDPOINT}/projects/${projectId}`);
      setOriginalProject(projectRequest.data);
      setProject(projectRequest.data);
        
    }
    getProject();
  }, [props]);

  const checkProjectChanged = (newProject) => setHasChanged(JSON.stringify(originalProject) !== (JSON.stringify(newProject||project)));
  const editProject = field => event => {
    const newProject = {
      ...project,
      [field]: event.target.value
    };
    checkProjectChanged(newProject)
    setProject(newProject)
  }

  const confirmReversion = confirmation => {
    setProject(originalProject);
    setHasChanged(false);
  }

  const updateProject = async () => {
    setIsLoading(true);
    const projectId = project.id;
    try {
      const httpPut = await axios.put(
        `${MPT_API_ENDPOINT}/projects/${projectId}`,
        JSON.stringify(project),
        {
          headers: {
            'Content-Type': 'application/json',
          }
        }
      );
      if (httpPut.status !== 200) return;
      setHasChanged(false);
    } catch (error) {

    } finally {
      setIsLoading(false);
      setIsSaved(true);  
    }
  }

  const saveNewProject = async () => {
    setIsLoading(true);
    const httpPost = await axios.post(
      `${MPT_API_ENDPOINT}/projects`,
      JSON.stringify(project),
      {
        headers: {
          'Content-Type': 'application/json',
        }
      }
    );
    setIsLoading(false);
    setIsSaved(true);
    if (httpPost.status !== 200) return;
    setHasChanged(false);
    const postData = httpPost.data;
    if (!postData.id) return;
    
    props.history.push(`/projects/${slug(postData.name)}/${postData.id}`);
  }

  const classes = useStyles();
  return (<>
    <AppBar position="sticky">
      <Toolbar style={{ justifyContent: 'space-between' }}>
        <div className={classes.AppBarGroup}>
          <IconButton
            className={classes.BackButton}
            disabled={originalProject && hasChanged}
            onClick={clickEvent => props.history.push(`/`)}>
            <ArrowBack />
          </IconButton>
          <Typography variant="h6" noWrap>
            Project info
          </Typography>
        </div>
        <div className={classes.AppBarGroup}>
          {originalProject && <div className={classes.loadingButton}>
            <Button
              variant="contained"
              color="default"
              onClick={event => confirmReversion()}
              disabled={!hasChanged || isLoading}
              className={classes.rightIcon}>
                <Restore className={classes.iconSmall} />Revert
            </Button>
          </div>}
          <div className={classes.loadingButton}>
            <Button
              variant="contained"
              color="secondary"
              disabled={!hasChanged || isLoading}
              className={classes.rightIcon}
              onClick={originalProject? updateProject : saveNewProject}>
                <Save className={classes.iconSmall} />Save
            </Button>
            {isLoading && <CircularProgress size={24} className={classes.LoadingIndicator} />}
          </div>
        </div>
      </Toolbar>
    </AppBar>
    <main>
      <Container maxWidth="lg">
        <Paper className={classes.Paper}>
          <Grid container spacing={3} alignItems="flex-end">
              <Grid item sm={12}>
                <Typography variant="h3" container="h1">Project info</Typography>
                <Typography variant="body1">Shows basic information about the project. You may edit the project information on this page. Any unsaved changes will be lost.</Typography>
              </Grid>
              <Grid item className={classes.FormField} sm={8} xs={12}>
                <TextField
                  label="Project name"
                  id="name"
                  className={classes.AlignWithSelect}
                  value={project.name || ''}
                  onChange={editProject('name')}
                  margin="normal"
                />
              </Grid>
              <Grid item className={classes.FormField} sm={4} xs={12}>
                <FormControl className={classes.FormItem}>
                  <InputLabel htmlFor="status">Status</InputLabel>
                  <Select
                    value={project.status || "pending"}
                    onChange={editProject('status')}
                    inputProps={{
                      name: 'status',
                      id: 'status',
                    }}
                  >
                    <MenuItem value={"pending"}>pending</MenuItem>
                    <MenuItem value={"in progress"}>in progress</MenuItem>
                    <MenuItem value={"on hold"}>on hold</MenuItem>
                    <MenuItem value={"complete"}>complete</MenuItem>
                    <MenuItem value={"cancelled"}>cancelled</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
          </Grid>
        </Paper>
        <Paper className={classes.Paper}>
          <Grid container spacing={3} alignItems="flex-end">
              <Grid item sm={12}>
                <Typography variant="h3" container="h2">Activities</Typography>
                <Typography variant="body1">The activities that are documented for this project.</Typography>
              </Grid>
              <Grid item className={classes.FormField} xs={12}>
                <MaterialTable
                  columns={[
                    { title: 'Activity', field: 'name', searchable: true },
                    { title: 'Description', field: 'description' },
                    {
                      title: 'Status',
                      field: 'status',
                      lookup: {
                        'pending': 'pending',
                        'in progress': 'in progress' },
                    },
                    { title: 'Commencement', field: 'start', type: 'date' },
                    { title: 'Completion', field: 'end', type: 'date' },
                    { 
                      title: 'Budget allocation',
                      field: 'budgetAllocation',
                      type: 'number'
                    }
                  ]}
                  editable={{
                    onRowAdd: newActivity =>
                      new Promise(async (resolve, reject) => {
                        newActivity.tempId = uuid();
                        newActivity.isNew = true;

                        const newProject = {
                          ...project,
                          activities: [
                            ...project.activities,
                            newActivity
                          ]
                        };
                        setProject(newProject);
                        checkProjectChanged(newProject);
                        resolve();
                      }),
                    onRowUpdate: (updatedActivity, oldActivity) =>
                      new Promise(async (resolve, reject) => {
                        const activities = project.activities.map((activity) => {
                          if (activity.id !== oldActivity.id) return activity;
                          return updatedActivity;
                        });

                        const newProject = {
                          ...project,
                          activities: activities
                        };

                        setProject(newProject);
                        checkProjectChanged(newProject);
                        resolve();
                      }),
                    onRowDelete: oldActivity =>
                      new Promise(async (resolve, reject) => {
                        const activities = project.activities.filter(activity => (activity.id !== oldActivity.id));
                        const newProject = {
                          ...project,
                          activities: activities
                        };

                        setProject(newProject);
                        checkProjectChanged(newProject);
                        resolve();
                      })
                  }}
                  data={project.activities}
                  title={`Activities`}
                  options={{
                    actionsColumnIndex: -1
                  }}
                  style={{ width: '100%' }}
                />
              </Grid>
          </Grid>
        </Paper>
      </Container>
    </main>
  </>)
}