import React from 'react';
import axios from 'axios';
import slug from 'slug';
import { Grid, Typography, Container, makeStyles, AppBar, Toolbar, Button } from '@material-ui/core'
import { Add } from '@material-ui/icons';

import MaterialTable from 'material-table'

const MPT_API_ENDPOINT = process.env.REACT_APP_MPT_API_ENDPOINT;

const useStyles = makeStyles(theme => ({
  Heading: {
    marginTop: theme.spacing(4),
    marginBottom: theme.spacing(4),
  },
  rightIcon: {
    marginLeft: theme.spacing(1),
  },
  iconSmall: {
    fontSize: 20,
    marginRight: 4
  },
  BackButton: {
    marginRight: theme.spacing(1),
    color: 'white'
  },
  AppBarGroup: {
    display: 'flex',
    alignItems: 'center',
    maxWidth: '50%' 
  },
}))

export default function Projects(props) {
  const styles = useStyles();

  return (
    <>
    <AppBar position="static">
      <Toolbar style={{ justifyContent: 'space-between' }}>
        <div className={styles.AppBarGroup}>
          <Typography variant="h6" noWrap>
            Kirtipur Municipality
          </Typography>
        </div>
        <div className={styles.AppBarGroup}>
          <div className={styles.loadingButton}>
            <Button
              variant="contained"
              color="secondary"
              onClick={() => props.history.push('projects/new')}
              className={styles.rightIcon}>
                <Add className={styles.iconSmall} />Add new project
            </Button>
          </div>
        </div>
      </Toolbar>
    </AppBar>
    <main>
      <Container maxWidth="lg">
        <Grid container>
          <Grid item xs={12} md={8}>
            <Typography className={styles.Heading} variant="h1" container="h1">Projects</Typography>
            <Typography className={styles.Heading} variant="body1">Below is a list of municipality projects that are being managed by the Municipality Project Tracker. Click on a project to view all of its details and activities.</Typography>
          </Grid>
          <Grid item xs={12} md={4}>
            
          </Grid>
          <Grid item xs={12}>
              <MaterialTable
                columns={[
                  { title: 'Project name', field: 'name', searchable: true },
                  { title: 'Ward', field: 'wardNumber', type: 'numeric' },
                  { title: 'Activities', field: 'activitiesCount', type: 'numeric' },
                  { title: 'Status', field: 'status' }
                ]}
                data={query =>
                  new Promise(async (resolve, reject) => {
                    const projectsRequest = await axios(`${MPT_API_ENDPOINT}/projects/with-activities`);
                    
                    if ( ! projectsRequest.data)
                      reject("Error fetching projects");

                    const projects = projectsRequest.data.map(project => ({
                      ...project,
                      activitiesCount: project.activities.length
                    }));
                    resolve({
                      data: projects,// your data array
                      page: 0,// current page number
                      totalCount: projects.length // total page number
                    });
                  })
                }
                title="Active projects"
                onRowClick={
                  (event, project) => props.history.push(`projects/${slug(project.name)}/${project.id}`)
                }
                actions={[
                  {
                    icon: 'edit',
                    tooltip: 'Edit project',
                    onClick: (event, project) => props.history.push(`projects/${slug(project.name)}/${project.id}`)
                  },
                  rowData => ({
                    icon: 'delete',
                    tooltip: 'Delete project',
                    onClick: (event, project) => window.confirm("You want to delete " + project.name),
                    disabled: rowData.birthYear < 2000
                  })
                ]}
                options={{
                  actionsColumnIndex: -1
                }}
              />
          </Grid>
        </Grid>
      </Container>
    </main>
    </>)
}